import bunyan from 'bunyan';

const onsConfig = {
  accessKey: '699Ib2Y7IxxPitnM',
  secretKey: '1cDvdFJ5YnhkPxPsXBirMhDMR226hu',
  topicId: 'bmoji_api_mq_test',
  producerId: 'PID_api_test_1',
  consumerId: 'CID_api_test_1',
};

const logger = bunyan.createLogger({
  name: 'node-test',
  streams: [{
    stream: process.stdout,
    level: 'debug',
  }],
});

const Consumer = require('ons').Consumer;
const consumer = new Consumer('CID_api_test_alarm', onsConfig.topicId, 'tag1', onsConfig.accessKey, onsConfig.secretKey);
consumer.on('message', (msg, ack) => {
  logger.debug(msg);
});
consumer.init((err) => {
  if (err) {
    return logger.error(err, 'Oops... consumer1 init failed');
  }

  consumer.listen();
});
