import bunyan from 'bunyan';
require('./consumer');
require('./consumer1');

const SegfaultHandler = require('segfault-handler');
SegfaultHandler.registerHandler("crash.log");

const logger = bunyan.createLogger({
  name: 'node-test',
  streams: [{
    stream: null,
    path: '/data1/log/wedeal.api/bunyan.log',
    level: 'debug',
  }],
});

const onsConfig = {
  accessKey: '699Ib2Y7IxxPitnM',
  secretKey: '1cDvdFJ5YnhkPxPsXBirMhDMR226hu',
  topicId: 'bmoji_api_mq_test',
  producerId: 'PID_api_test_1',
  consumerId: 'CID_api_test_1',
};

const Producer = require('ons').Producer;
const producer = new Producer(onsConfig.producerId, onsConfig.accessKey, onsConfig.secretKey);
producer.start((err) => {
  if (err) {
    return console.error(err);
  }

  console.log('Producer Started!');
  producer.send(onsConfig.topicId, 'tag', {key: 'value'}, 3000, (err) => {
    if (err) {
      return console.error(err, 'Oops... addJob');
    }
  });

  producer.send(onsConfig.topicId, 'tag1', {key1: 'value1'}, 3000, (err) => {
    if (err) {
      return console.error(err, 'Oops... addJob1');
    }
  });
});

// const Consumer = require('ons').Consumer;
// const consumers = {};

// export default class QueueController {
//   static registerJob(jobName, handle) {
//     let consumer = consumers[jobName];
//     if (!consumer) {
//       consumer = new Consumer(onsConfig.consumerId, onsConfig.topicId, jobName/* TAGS */, onsConfig.accessKey, onsConfig.secretKey);
//       consumer.on('message', (msg, ack) => {
//         handle(msg).then(() => ack.done()).catch(() => ack.done(false));
//       });
//       consumer.init((err) => {
//         if (err) {
//           return console.error(err, 'Oops... consumer init failed');
//         }

//         consumer.listen();
//       });
//       consumers[jobName] = consumer;
//     }
//   }

//   static addJob(jobName, params, delay = 0) {
//     producer.send(`${jobName}_${JSON.stringify(params)}`, onsConfig.topicId, jobName, params, delay, (err) => {
//       if (err) {
//         return console.error(err, 'Oops... addJob');
//       }
//     });
//   }
// }

// QueueController.registerJob('job1', (msg) => {
//   console.log('job1 process: ' + msg);
//   return Promise.resolve();
// });
// QueueController.registerJob('job2', (msg) => {
//   console.log('job1 process: ' + msg);
//   return Promise.resolve();
// });

// // QueueController.registerJob('job2', (msg) => {
// //   console.log('job2 process: ' + msg);
// //   return Promise.reject(new Error('job2 error'));
// // });

// setTimeout(() => {
//   QueueController.addJob('job1', {key: 'value'});
//   // QueueController.addJob('job2', {key: 'value'});
// }, 3000);
